<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobileCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::defaultStringLength(191);
        Schema::create('mobileCategory', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subCategoryImage');
            $table->string('subCategoryID');
            $table->string('subCategoryImage2');
            $table->string('subCategoryImage3');
            $table->string('subCategoryImage4');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('mobileCategory');
    }
}
