<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSliderImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::defaultStringLength(191);
        Schema::create('Image', function (Blueprint $table) {
            $table->increments('id');
            $table->string('attributeID');
            $table->string('bannerImage');
            $table->string('CategoryID');
            $table->string('ImageSection');
            $table->string('tagID');
            $table->string('ProductID');
            $table->string('customPageID');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('Image');

    }
}
