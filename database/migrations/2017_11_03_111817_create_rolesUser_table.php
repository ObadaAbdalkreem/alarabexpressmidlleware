<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
          Schema::defaultStringLength(191);
        Schema::create('rolesUsers', function (Blueprint $table) {
        $table->increments('id');
            $table->string('name')->unique();
            $table->string('display_name');
                        $table->timestamps();

                    });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('rolesUsers');
    }
}
