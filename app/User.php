<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
 use Notifiable, HasApiTokens;
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
         protected $table = 'users';

    protected $fillable = [
        'name', 'email', 'password','role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // for authorization obada
    public function getPermisions() {
        //$d = $this->group_id ;
        //$group_id = Groups::find($d);
         //$resultr = Groups::select('name')->where('id',$d)->first();
            //   $roles= roles::select('name')->where('id',1)->first();
         // $result = Rights::select('name')->where('id',$rights->Right_id)->first();
         $d=$this->id ;
         $roles=User::select('role_id')->where('id',$d)->first();
        return  $roles->role_id;//$resultr->name ;
        /*
   $rights = select rights from group_right 
         where group_id = $grroup_id 
         select name from rights 
         where right = $rights 
*/
    }
}
