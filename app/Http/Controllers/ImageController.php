<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;
use App\Http\Controllers\DB;
use Response;
class ImageController extends Controller
{
    //
    public function index() 
    {
        $result=[];
        $image = Image::all() ;
        $i = 0 ; 
        for ( $i  ; $i < count($image) ; $i++)
        {
             $result [$i]= [
        $image[$i]->ImageSection=> [
                'attributes'=>[
                'attributeID'=>$image[$i]->id,
                'bannerImage'=>$image[$i]->bannerImage,
                'CategoryID'=>$image[$i]->CategoryID,
                'tagID'=>$image[$i]->tagID,
                'ProductID'=>$image[$i]->ProductID,
                'customPageID'=>$image[$i]->customPageID,
                ]
            ]
        ];
            
        }
        
        return $image; 
    }
    public function uploadImage (Request $request ) 
    {
        $image = new Image ; 
        $exploded=explode(',',$request->input('image') );
        $decode = base64_decode($exploded[1]);
        $inputs = $request->all ();
        $file_name='';
        if (str_contains($exploded[0],'jpeg'))
            $extension='jpg';
        else 
            $extension='png'; 

        $fileName = str_random().'.'.$extension; 
        $path = public_path().'/storage/images'.$fileName ; 
        file_put_contents($path,$decode);    
        $result  = $image->create( //$request->except('image')+[
                   [ 'attributeID'=> 'test',
                    'bannerImage'=>$fileName,
                    'CategoryID'=>$request->input('CategoryID'),
                    'tagID'=>$request->input('tagID'),
                    'ProductID'=>$request->input('ProductID'),
                    'customPageID'=>$request->input('customPageID'),
                    'ImageSection'=>$request->input('ImageSection')
                    ] );
        // $path = $request->file('image')->store('public/images');
        // if ($request->hasFile('image'))
        // {
        //     // $path = $request->file('image')->store('image');
        //     // $file_name = $request->file('image')->getClientOriginalName();
            
        // }
        // $result = $image->create($inputs);
        // $image->update(['bannerImage'=>'test']);
        // if($result->save()){
        //     return $path;
        // } else{
        //     return "failed";
        // }   
            return $result;
    }

    public function update_content(Request $request,$id) 
    {

        $image = Image::find($id);
        

        $image->attributeID='test';
        if ($request->input('image') != null)
        {
         $exploded=explode(',',$request->input('image') );
        $decode = base64_decode($exploded[1]);
        $inputs = $request->all ();

        $file_name='';
        if (str_contains($exploded[0],'jpeg'))
            $extension='jpg';
        else 
            $extension='png'; 

        $fileName = str_random().'.'.$extension; 
        $path = public_path().'/storage/images'.$fileName ; 
        file_put_contents($path,$decode);        
        $image->bannerImage=$fileName;
        }
        $image->CategoryID=$request->input('CategoryID');
        $image->tagID=$request->input('tagID');
        $image->ProductID=$request->input('ProductID');
        $image->customPageID=$request->input('customPageID');
        $image->ImageSection=$request->input('ImageSection');
         $image->save();
        return $image;
    }

    public function getImage($id)
    {
        $image= Image::find($id);
        return $image;
    }

    public function getallBanners() 
    {

        $images = Image::all() ;
        $finalImages = [];

        foreach($images as $img){
            $finalImages[$img["ImageSection"]] = $img;
        }

        return Response::json($finalImages);
        
        //$result=[];
    //     
    //     $i = 0 ; 
	// //$test= \DB::select('SELECT * FROM Image');;
    //     for ( $i  ; $i < count($image) ; $i++)
    //     {
    //          $key = $image[$i]->ImageSection;
    //         $data[]=[
    //             $key=>$image[$i]->bannerImage
    //         ];
	//  $key2=$i.'obada';
    //       $data3[]=[
	// 	$key2=>'test'	
	//    ];	   
	//    $data1[]=(object)array($key=>$image[$i]->bannerImage); 	
    //          $result[$i] = [
    //              $key => [                
    //             'attributeID'=>$image[$i]->id,
    //             'bannerImage'=>$image[$i]->bannerImage,
    //             'CategoryID'=>$image[$i]->CategoryID,
    //             'tagID'=>$image[$i]->tagID,
    //             'ProductID'=>$image[$i]->ProductID,
    //             'customPageID'=>$image[$i]->customPageID,
    //             ]
            
    //     ];
         
	// //$data[]=(object)array($key=>$result);   
    //     }
    //    $collection= collect($data1); 
    //     return Response::json(array($result));//response()->json($result);
    
//     $arr[] = array("id"=>1,"ImageSection"=>"slider1","text"=>"text 1");
// $arr[] = array("id"=>2,"ImageSection"=>"slider2","text"=>"text 2");

// return Response::json((object)$arr);
    }
}
