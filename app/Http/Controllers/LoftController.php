<?php

namespace App\Http\Controllers;
use App\lofts;
use App\User;
use App\loft_specifications;
use App\Specification;
use App\meta_datas;
use App\rates;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use Auth;
use Gate;
class LoftController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
       $user = Auth::guard('api');
        if (Gate::allows('LoftDetails', $user)){
            $lofts = lofts::all();
        if($lofts->isEmpty()){
            return 'no lofts found !!';
        } else {
            return $lofts;
        }
    }else {
        return 'Fails not authorized';//'you are not authorized';
    }
        //return "obada";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function metaData($id) // metadata for loft
    {
        //
        $metaData = meta_datas::where('entityName','Loft')->where('idForign',$id)->first();
        $metaData->LoftImage = base64_encode($metaData->LoftImage);
        $img = 'data:image/jpeg;base64,'.$metaData->LoftImage;
            $imgData = base64_encode(file_get_contents($img));
            $metaData->LoftImage = $src = 'data: '.mime_content_type($img).';base64,'.$imgData;
            return $metaData;
    }
    
    /*********Return Loft Rate************/
    public function Loft_Rates($id) // metadata for loft
    {
        //
        $RatesData = rates::where('entityName','Loft')->where('idForign',$id)->first();
            return $RatesData;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     /*********To Add Loft************/
    public function store(Request $request)
    {
        //
        $loft = new lofts ;
        $loft->IsActive=$request->input('IsActive'); 
        $loft->discreption=$request->input('discreption');
        if ($loft->save()){
            return "Success Post loft Spec";
        }
        else {
            return "faild to post loft spec";
        }
    

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Loft  $loft
     * @return \Illuminate\Http\Response
     */
    public function show(lofts $loft , $id)
    {
        //input (id loft) .. response : array of specification loft-spec ,, and Spec data 
        $loft = lofts::find($id); 
        if ($loft->IsActive==1) {
        /*$loft_Specification = loft_specifications::where('id_loft',$loft->id)->get();
        return $loft_Specification;*/
        $loft_SpecificationInfo = loft_specifications::join('specifications', 'loft_specifications.id_Spec', '=', 'specifications.id')
        ->where('loft_specifications.id_loft','=',$loft->id)->get();
        //->where('patients.clinic_id', $user->clinic_id)
        //->where('visits.clinic_id', $user->clinic_id)
        //->where('visits.active','=','1')
       return $loft_SpecificationInfo;
        }
        else {
            return "this loft not active";
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Loft  $loft
     * @return \Illuminate\Http\Response
     */
    public function edit(Loft $loft)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Loft  $loft
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Loft $loft)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Loft  $loft
     * @return \Illuminate\Http\Response
     */
    public function destroy(Loft $loft)
    {
        //
    }
}
