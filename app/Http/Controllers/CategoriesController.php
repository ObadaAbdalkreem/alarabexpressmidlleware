<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Response;
use App\mobileCategory;

class CategoriesController extends Controller
{

    function getCategories(Request $request){
        $queryStrings = $request->query();
        $language = "&lang=ar";
        foreach ($queryStrings as $key => $value){
            if ($key == "lang"){
                $language = "&lang=" . $value;
            }
        }

        $client = new \GuzzleHttp\Client();
        $tokenReq = $client->request('POST', 'http://3arabuy.com/wp-json/jwt-auth/v1/token',[
            'json' => ['username' => 'admin', "password" => "TestEmail"]
        ]);

        $tokenVal = json_decode($this->rmUnwantedChar($tokenReq), true)['token'];

        $res = $client->request('GET', 'http://3arabuy.com/wp-json/wc/v2/products/categories?per_page=100' . $language, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer '.$tokenVal
            ]
        ]);


        $json = json_decode($this->rmUnwantedChar($res), true);

        return $this->reorderCategories($json);

    }

    function rmUnwantedChar($res){
        $body = (string)$res->getBody();

        if (0 === strpos(bin2hex($body), 'efbbbf')) {
           $body = substr($body, 3);
        }

        return $body;
    }

    function findSubCategories(&$mainCat, $cateId){

        foreach($mainCat as $key => $value){
            if(!isset($value['id'])){
                continue;
            }

            $s_categoryId = $value['id'];

            if($s_categoryId != $cateId && $cateId == $value['parent']){

                $this->findSubCategories($mainCat, $s_categoryId);

                if(!isset($mainCat[$cateId]['subCategories'])){
                    $mainCat[$cateId]['subCategories'] = array();
                }
                $mainCat[$key]['isUnderCategory'] = true;
                array_push($mainCat[$cateId]['subCategories'], $mainCat[$key]);

            }
        }

    }

    function reorderCategories($categories){

    	$mainCat = array();// associative array
    	//fill associative array
    	foreach($categories as $i => $v) {
            $mainCat[$v['id']] = $v;
        }

    	// append subcategories under each parent.
        foreach ($mainCat as $key => $value) {
            $this->findSubCategories($mainCat, $value['id']);
        }
         

        //remove categories that dose not have subcategories
        foreach ($mainCat as $key => $value) {
        	if(isset($value['isUnderCategory']) || (!isset($value['isUnderCategory']) && !isset($value['subCategories']) && $value['parent'] != 0) ){
    			unset($mainCat[$key]);
        	}
        }

        return json_encode(array_values($mainCat));
    }

    public function mobileCategory(Request $request) 
    {
        $mobileCategory = new mobileCategory;
        $exploded=explode(',',$request->input('subCategoryImage'));
        $exploded2=explode(',',$request->input('subCategoryImage2'));
        $exploded3=explode(',',$request->input('subCategoryImage3'));
        $exploded4=explode(',',$request->input('subCategoryImage4'));
        $decode = base64_decode($exploded[1]);
        $decode2 = base64_decode($exploded2[1]);
        $decode3 = base64_decode($exploded3[1]);
        $decode4 = base64_decode($exploded4[1]);
        $inputs = $request->all ();
        $file_name='';
        $file_name2='';
        $file_name3='';
        $file_name4='';
        if (str_contains($exploded[0],'jpeg'))
            $extension='jpg';
        else 
            $extension='png'; 

        if (str_contains($exploded2[0],'jpeg'))
            $extension2='jpg';
        else 
            $extension2='png'; 
    
        if (str_contains($exploded3[0],'jpeg'))
            $extension3='jpg';
        else 
            $extension3='png'; 

        if (str_contains($exploded4[0],'jpeg'))
            $extension4='jpg';
        else 
            $extension4='png'; 
 
        $fileName = str_random().'.'.$extension; 
        $fileName2 = str_random().'.'.$extension2; 
        $fileName3 = str_random().'.'.$extension3; 
        $fileName4 = str_random().'.'.$extension4; 
        $path = public_path().'/storage/images'.$fileName ; 
        $path2 = public_path().'/storage/images'.$fileName2 ; 
        $path3 = public_path().'/storage/images'.$fileName3 ; 
        $path4 = public_path().'/storage/images'.$fileName4 ; 
        file_put_contents($path,$decode);
        file_put_contents($path2,$decode2);
        file_put_contents($path3,$decode3);
        file_put_contents($path4,$decode4);    
        $result  = $mobileCategory->create( //$request->except('image')+[
                   [ 
                    'subCategoryImage'=>$fileName,
                    'subCategoryImage2'=>$fileName2,
                    'subCategoryImage3'=>$fileName3,
                    'subCategoryImage4'=>$fileName4,
                    'subCategoryID'=>$request->input('subCategoryID')
                    ] );

         return $result;            
    }

    public function getmobileCategoy()
    {
        $mobileCategory =  mobileCategory::all();
        $finalmobilecategory=[];
        foreach ($mobileCategory as $img )
        {
         //$finalmobilecategory[$img["id"]]= $img;
         $finalmobilecategory= $img;
        }
        $main_subCateegory =$this->getCategories();
        return Response::json($finalmobilecategory);
    }

    public function getmobileImageSubCategoy()
    {
        $mobileCategory =  mobileCategory::all();
        $finalmobilecategory=[];
        foreach ($mobileCategory as $img )
        {
         //$finalmobilecategory[$img["id"]]= $img;
         $finalmobilecategory= $img;
        }
        return $mobileCategory;//Response::json($finalmobilecategory->id);
    }


    function getCategories_mobile(Request $request){

        $queryStrings = $request->query();
        $language = "&lang=ar";
        foreach ($queryStrings as $key => $value){
            if ($key == "lang"){
                $language = "&lang=" . $value;
            }
        }

        $client = new \GuzzleHttp\Client();     
        $tokenReq = $client->request('POST', 'http://3arabuy.com/wp-json/jwt-auth/v1/token',[
            'json' => ['username' => 'admin', "password" => "TestEmail"]
        ]);

        $tokenVal = json_decode($this->rmUnwantedChar($tokenReq), true)['token'];
        
        $res = $client->request('GET', 'http://3arabuy.com/wp-json/wc/v2/products/categories?per_page=100' . $language, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer '.$tokenVal
            ]
        ]);

        $json = json_decode($this->rmUnwantedChar($res), true);

        return $this->reorderCategories_mobile($json);

    }

    function reorderCategories_mobile($categories){

    	$mainCat = array();// associative array

    	//fill associative array
    	foreach($categories as $i => $v){
    		$mainCat[$v['id']] = $v;
        }

    	// append subcategories under each parent.
        foreach ($mainCat as $key => $value) {
        	$parentId = $value['parent'];
            
        	if ( $parentId != "0") {    
    			if(!isset($mainCat[$parentId]['subCategories'])){
    				$mainCat[$parentId]['subCategories'] = array();
                  //  $mainCat[$parentId]['subCategoriesImage'] = array();
    			}
                $mainCat[$key]['isUnderCategory'] = true;
                array_push($mainCat[$parentId]['subCategories'], $value);
                //$this->trace_subCategory($parentId,$mainCat[$parentId]['subCategories'],$mainCat);
               $mainCat[$parentId]['subCategories'] =  $this->trace_subCategory($parentId, $mainCat[$parentId]['subCategories']);
              
    		}
           //  $this->trace_subCategory($mainCat[$key]['subCategories']);
        }
        /*$ff= array();
        $finalsubmobilecategory=[];
        $subCategoryImage = $this->getmobileImageSubCategoy();
        //\Log::info($subCategoryImage);
        foreach($mainCat as $key => $value){
    		// $subCategory[$v['subCategories']['id']] = $v;
            // $subCategory = $value['subCategories']['id'];
            
           if(isset($mainCat[$key]['subCategories'])){
                $t= $mainCat[$key]['subCategories'];
            //    $t[$i]['subCategoryImage']=array();
            //      if(isset($t[$i]['id'])){
            //     if ($subCategoryImage->subCategoryID == $t[$i]['id']){
            //         array_push($t[$i]['subCategoryImage'], 'OBADAKKK');
            //     }
            //     }
               // \Log::info($t);
                // array_push($t[$i]['obada'], 1);
             foreach ($t as $i => $v){
                    //$ff[]
                    //array_push($t[$i]['obada'], 1);
                foreach ($subCategoryImage as $subImage){
                       // $finalsubmobilecategory=$subImage;
                    //    \Log::info('yekoskeow');
                    // \Log::info($subImage->subCategoryID);

                       
                if ($subImage->subCategoryID == $t[$i]['id']){
                    $t[$i]['subCategoryImage']=array();
                    // $data2[] = [
                    //         'label' => $t,
                    //         'subImage' =>$subImage->subCategoryImage
                    //     ];
                    array_push($t[$i]['subCategoryImage'], $subImage->subCategoryImage);
                    array_push($t[$i]['subCategoryImage'], $subImage->subCategoryImage2);
                    array_push($t[$i]['subCategoryImage'], $subImage->subCategoryImage3);
                    array_push($t[$i]['subCategoryImage'], $subImage->subCategoryImage4);
                    //\Log::info('sucssssssssssssssssss obada');
                }
                   // array_push($ff, $t[$i]['id']);
                
                    
                }
                    }
                    \Log::info($t);
             //  \Log::info('obada Test subcategor',$t[$i]['id']);
            
        }
        }   */

        //remove categories that dose not have subcategories
       // if(isset($value['isUnderCategory']) || (!isset($value['isUnderCategory']) && !isset($value['subCategories']) && $value['id'] != 0) ){
        foreach ($mainCat as $key => $value) {
                if(isset($value['isUnderCategory']) || (!isset($value['isUnderCategory']) && !isset($value['subCategories']) && $value['parent'] != 0) ){
            	//if(!isset($mainCat[$key]['subCategories'])){
    			unset($mainCat[$key]);
        	}
        }

        return json_encode(array_values($mainCat));
    }

    function trace_subCategory($parentId,$subCategory_mob)
    {
                 $parent_id = $parentId;  
                // $mainCat=$mainCat; 
                 $recordsubCategory= $subCategory_mob;//$mainCat[$key]['subCategories'];
                // \Log::info($recordsubCategory);
                 $subCategoryImage = $this->getmobileImageSubCategoy();
                 
                 foreach ($recordsubCategory as $i => $v)
                 {
                     foreach ($subCategoryImage as $subImage)
                     {
                        if ($subImage->subCategoryID == $recordsubCategory[$i]['id']){
                        $recordsubCategory[$i]['subCategoryImage']=array();
                        array_push($recordsubCategory[$i]['subCategoryImage'], $subImage->subCategoryImage);
                        array_push($recordsubCategory[$i]['subCategoryImage'], $subImage->subCategoryImage2);
                        array_push($recordsubCategory[$i]['subCategoryImage'], $subImage->subCategoryImage3);
                        array_push($recordsubCategory[$i]['subCategoryImage'], $subImage->subCategoryImage4);
                    \Log::info('sucssssssssssssssssss obada',$recordsubCategory);
                        }
                       // array_push($mainCat[$parent_id]['subCategories'], $recordsubCategory);
                     } 
                   // \Log::info($mainCat);   
                   // \Log::info('sucssssssssssssssssss obada',$recordsubCategory);  
                 }
                 return $recordsubCategory;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
