<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mobileCategory extends Model
{
    //
    protected $table = 'mobileCategory';
    protected $fillable = ['id','subCategoryImage','subCategoryImage2','subCategoryImage3','subCategoryImage4' ,'subCategoryID'];
}
