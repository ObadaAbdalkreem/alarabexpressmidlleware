<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    //
     protected $table = 'Image';
    protected $fillable = ['id','ImageSection' ,'attributeID', 'bannerImage','CategoryID','tagID','ProductID','customPageID'];

}
