<?php

namespace App\Policies;

use App\User;
use App\lofts;
use Illuminate\Auth\Access\HandlesAuthorization;

class LoftPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the loft.
     *
     * @param  \App\User  $user
     * @param  \App\Loft  $loft
     * @return mixed
     */
    public function view(User $user, Loft $loft)
    {
        //
    }

    /**
     * Determine whether the user can create lofts.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        if ($user->getPermisions() == 1)
        {
            return true ;
        }
        else {
            return false ;
        }
    }

    /**
     * Determine whether the user can update the loft.
     *
     * @param  \App\User  $user
     * @param  \App\Loft  $loft
     * @return mixed
     */
    public function update(User $user, Loft $loft)
    {
        //
    }

    /**
     * Determine whether the user can delete the loft.
     *
     * @param  \App\User  $user
     * @param  \App\Loft  $loft
     * @return mixed
     */
    public function delete(User $user, Loft $loft)
    {
        //
    }
}
