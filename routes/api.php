<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
    });
	Route::resource('Specification', 'SpecificationController');
// Route::get('/lofts', 'test@index')->middleware('auth:api');
Route::post('/uploadImage', 'ImageController@uploadImage');//->middleware('auth:api'); for loft_specification and specification data
// Route::post('/AddLoft_Spec/{id1}/{id2}','SpecificationController@store');
Route::post('/updatecontent/{id}', 'ImageController@update_content');//->middleware('auth:api'); for loft_specification and specification data
Route::get('/getImage/{id}', 'ImageController@getImage');//->middleware('auth:api'); for loft_specification and specification data
Route::get('/getallBanners','ImageController@getallBanners');
Route::get('/index', 'ImageController@index');
Route::get('/test', 'TestController@index');
//Route::get('test', 'CheckRole@test');//->middleware('auth:api');

Route::get('categories', 'CategoriesController@getCategories');//, array('only' => array('index'))
Route::post('/mobileCategory', 'CategoriesController@mobileCategory');
Route::get('/getmobileCategory', 'CategoriesController@getmobileCategoy');
Route::get('/getsubcategories_mobile', 'CategoriesController@getCategories_mobile');//, array('only' => array('index'))
Route::get('/iyad/cate', 'CategoriesController@getCategories');//, array('only' => array('index'))
